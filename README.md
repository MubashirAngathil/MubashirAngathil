<!--Section 1  -->
<img src='./Assets/hacker-thinking-about-code.gif' alt='coding....' width='300' align='right' />
<!-- <img src='./Assets/happy-hacker.gif' alt='coding....' width='400' align='right' /> -->
<h1 align='center'> Welcome to Mubashir's Profile</h1>
<!-- <img src="https://github.com/MubashirAngathil/typing-intro/blob/main/chat.svg" width="500"  align='left'> -->
<img src='https://komarev.com/ghpvc/?username=MubashirAngathil'  align='right'/>

<!--Section 2  -->
<h2 align='center'>To Find Out Me</h2>
<p align='right'>
  <a href='https://www.linkedin.com/in/mubashir-angathil/' target="_blank"><img src='/Assets/animation/linkedin.gif' width='64px' height='64px'/></a>
  <a href='https://www.instagram.com/mubzyr_ashraf/' target='_blank'><img src='/Assets/animation/instagram.gif' width='64px' height='64px'/></a>
  <a href='https://twitter.com/mubzyr_ashraf' target='_blank'><img src='/Assets/animation/twitter.gif' width='64px' height='64px'/></a>
  <img src='/Assets/animation/whatsapp (1).gif' width='64px' height='64px'/>
  <a href='https://github.com/MubashirAngathil' target='_blank'><img src='/Assets/normal/icons8-github-64.png' width='50px' height='50px'/></a>
  <a href='https://gitlab.com/MubashirAngathil' target='_blank'><img src='/Assets/normal/icons8-gitlab-48.png' width='50px' height='50=px'/></a>
  <a href='https://dev.to/mubashirangathil' target='_blank'><img src='/Assets/3d/dev.png' width='50px' height='50px'/></a>
  <a href='mailto: mubashirangathil5142@gmail.com'> <img src='/Assets/normal/icons8-gmail-48.png' width='50px' height='50px'/></a>
  <a href='https://t.me/mubashir_angathil'><img src='/Assets/normal/icons8-telegram-app-48.png' width='50px' height='50px'/></a>
 </p>

<!-- section 3  -->
<h2>Synopsis of Activites</h2>
<img width='1000' height='160'  src="https://github-readme-streak-stats.herokuapp.com?user=MubashirAngathil&count_private=true&show_icons=true&theme=dark&date_format=M%20j%5B%2C%20Y%5D&background=000000&stroke=045E61&ring=18CABF&fire=07DDD6&currStreakNum=FFFFFF&currStreakLabel=00DDD5&border=FFFFF&dates=0CAB31&hide_border=true" >

<img align='right' width='360' alt='top used languages' src="https://github-readme-stats.vercel.app/api/top-langs/?username=MubashirAngathil&langs_count=8&count_private=true&layout=compact&theme=vision-friendly-dark&hide_border=true" alt="Mr" />

<img width='430' src="https://github-readme-stats.vercel.app/api?username=MubashirAngathil&show_icons=true&theme=chartreuse-dark&background=000000&hide_border=true&count_private=true">

<!--Section 4 -->
<h2 align='center'>Languages</h2>
<p align='center'>
  <img src='/Assets/3d/nodejs.png' width='42px' height='42px'/>
  <img src='/Assets/3d/javascript.png' width='36px' height='36px'/>
  <img src='/Assets/normal/icons8-typescript-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-react-native-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-html-5-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-css3-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-dart-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-xml-64.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-java-64.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-c-programming-48.png' width='42px' height='42px'/>
</p>

<!--Section 5 -->
<h2 align='center'>Softwares</h2>
<p align='center'>
  <img src='/Assets/normal/icons8-visual-studio-code-2019-48.png' width='36px' height='36px'/>
  <img src='/Assets/normal/icons8-git-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-android-studio-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-console-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-github-48.png' width='42px' height='42px'/>
</p>
<!--Section 6 -->

<!--Section 7 -->
<h2 align='center'>Databases</h2>
<p align='center'>
  <img src='/Assets/normal/icons8-mongodb-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-firebase-48.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-mysql-logo-48.png' width='42px' height='42px'/>
  <img src='/Assets/3d/icons8-microsoft-sql-server-96.png' width='42px' height='42px'/>
</p>
<!--Section 8 -->
<h2 align='center'>Tools&Others</h2>
<p align='center'>
  <img src='/Assets/normal/postman.png' width='42px' height='42px'/>
  <img src='/Assets/normal/icons8-handlebar-mustache-60.png' width='42px' height='42px'/>
  <img src='/Assets/normal/flutter.png' width='40px' height='40px'/>
  <img src='/Assets/3d/android-logo.png' width='56px' height='46px'/>
</p>


<img  src="https://activity-graph.herokuapp.com/graph?username=MubashirAngathil&theme=react-dark&hide_border=true" alt="Mr" /> 

<!-- # Profile under updating.................. -->
